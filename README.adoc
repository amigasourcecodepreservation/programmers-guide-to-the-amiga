= Programmers Guide to the Amiga 

This is the digital version of the book `Programmers Guide to the Amiga`.

*Author:* Robert "Rob" Peck

*Year:* 1987

*Published:* Sybex

Released under *Creative Commons* 2018.

== Description

_The Programmer's Guide to the Amiga is a guided hands-on tour through the Amiga system, packed with in-depth information and sample programs you won't find anywhere else. This book is must for programmers, owners and software developers - anyone who wants to master and use the Amiga's unique capabilities_

Topics covered include:

* unique in-depth treatment of terminal-base programming on the Amiga
* step-by-step programming examples in every chapter illustrating proper use of system routines
* ready-to-use routines that simplify programming for superb graphics, animation, device control and much more
* an introduction to multitasking and message-passing procedures
* ....

== Read this book: 

* https://gitlab.com/amigasourcecodepreservation/programmers-guide-to-the-amiga/blob/master/pdf/programmers-guide-to-the-amiga-1987-peck.pdf[Scanned pdf]

You can also find the source code examples in the git repository

== How the books was released under Creative Commons

I sent a letter to the wife of the late Robert Peck, Andrea Peck, and asked for permission to release Robert's Amiga Books under our project. She gave me permission to release the books under Creative Commons, but I also had to check with the original publisher, Sybex, bought by Wiley in 2005. They were fine with it, even though the rights had already reverted to the author anyway. A great thanks to Andrea, and to Sybex/Wiley for making it possible. And of course, thanks to Robert "Rob" Peck whose contributions to the Amiga and tech scene will always be remembered.

== Contributing

If you would like to help out, here is a suggested to-do:

* Convert the original scan to AsciiDoc.
* Provide a better scan
 
== License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

== Copyright & Author

Robert "Rob" Peck, Andrea Peck © 1987 - 2018


